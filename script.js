const containerEl = document.querySelector(".container")
const gameContainer = document.getElementById("game");
const homePageEl = document.getElementById("homePage")
const startBtnEl = document.querySelector(".start-button")
const bestScoreEl = document.querySelector(".best-score")

let newWidth = window.innerWidth;

openFullscreen()

function openFullscreen() {
  if (document.requestFullscreen) {
    console.log("in")
    document.requestFullscreen();
  } else if (document.webkitRequestFullscreen) { /* Safari */
    document.webkitRequestFullscreen();
  } else if (document.msRequestFullscreen) { /* IE11 */
    document.msRequestFullscreen();
  }

}

// let flipel = document.querySelector(".flip-box");
// let flipInner = document.querySelector(".flip-box-inner");

// flipel.addEventListener("click", () => {
//   flipInner.classList.toggle("rotate");
// });


containerEl.addEventListener('touchmove', function (event) {
  if (event.scale !== 1) { event.preventDefault(); }
}, false);

const gifs = [
  "1.gif",
  "2.gif",
  "3.gif",
  "4.gif",
  "5.gif",
  "6.gif",
  "7.gif",
  "8.gif",
  "9.gif",
  "10.gif",
  "11.gif",
  "12.gif"
];


let gameTimer = 90;
let score = 0;
let timer
let timerCount = gameTimer;
let highScore = localStorage.getItem("highScore")
if (!highScore) {
  highScore = 0
}

//localStorage.clear()
// console.log(highScore)


//Home_page DOM
bestScoreEl.textContent = `Best score :-    ${highScore}`


//GamePage DOM
let gamePageEl = document.createElement("div")
let scoreContainerEl = document.createElement("div")
let highScoreEl = document.createElement("p")
highScoreEl.textContent = `High Score: `
let higscoreSpanEl = document.createElement("span")
higscoreSpanEl.textContent = highScore
highScoreEl.append(higscoreSpanEl)
let scoreEl = document.createElement("p")
scoreEl.textContent = "Your Score: "
let scoreSpanEl = document.createElement("span")
scoreSpanEl.textContent = score
scoreEl.append(scoreSpanEl)
scoreContainerEl.append(highScoreEl, scoreEl)
scoreContainerEl.classList.add("score-container")
gamePageEl.id = "gamePage";

const counterEl = document.createElement("p");
counterEl.innerHTML = `Timer: ${timerCount} Sec`
counterEl.classList.add("counter")
counterEl.id = "counter"

gamePageEl.append(scoreContainerEl, counterEl, gameContainer)
gamePageEl.classList.add("hide-container")
containerEl.append(gamePageEl)



//Success Page Modal DOM

let modalContainerEl = document.createElement("div")
modalContainerEl.classList.add("modal", "hide-container")
let modalContentEl = document.createElement("div")
modalContentEl.classList.add("modal-content")
let topContainer = document.createElement("div")
let imgContainer = document.createElement("div")
let textContainer = document.createElement("div")
let bottomContainer = document.createElement("div")
topContainer.append(imgContainer, textContainer);
topContainer.classList.add("top-container")
textContainer.classList.add("text-container")
imgContainer.classList.add("text-container")
let modalImg = document.createElement("img")
modalImg.classList.add("success-img")
imgContainer.append(modalImg)
let resultEl = document.createElement("h5")
resultEl.textContent = "Your results!!!!!!"
let finalScoreEl = document.createElement("p")
let newHighScoreText = document.createElement("p")
let timeTakenEl = document.createElement("p")
textContainer.append(resultEl, finalScoreEl, timeTakenEl)
let successEl = document.createElement("h1")
successEl.classList.add("success-text")
bottomContainer.classList.add("text-container2")
let restartBtnEl = document.createElement("button")
restartBtnEl.classList.add("restart-button")
restartBtnEl.textContent = "Home Page"
bottomContainer.append(newHighScoreText, successEl, restartBtnEl)
modalContentEl.append(topContainer, bottomContainer)
modalContainerEl.append(modalContentEl)
gamePageEl.append(modalContainerEl)


restartBtnEl.addEventListener("click", () => {
  modalContainerEl.classList.add("hide-container")
  gamePageEl.classList.add("hide-container")
  scoreContainerEl.classList.add("hide-container")
  counterEl.style.display = "none"
  homePageEl.classList.remove("hide-container")
  gameContainer.innerHTML = ""
  score = 0
  scoreSpanEl.textContent = `${score}`
  timerCount = gameTimer
})






startBtnEl.addEventListener("click", () => {
  counter()
  let finalGifArray = [...gifs, ...gifs]
  let shuffledGifArray = shuffle(finalGifArray)
  createDivsForGifs(shuffledGifArray)
  homePageEl.classList.add("hide-container")
  gamePageEl.classList.remove("hide-container")
  scoreContainerEl.classList.remove("hide-container")
  counterEl.style.display = "flex"

  gamePageEl.style.display = "flex"

})


function shuffle(array) {
  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}


let count = 0

function createDivsForGifs(gifArray) {

  console.log("shuffling")
  let firstColor = "#eeeed2"
  let secondColor = "#769656"
  let temp = ""
  gameContainer
  for (let gif of gifArray) {
    const newDiv = document.createElement("div");
    let imgEl = document.createElement("img")
    let id = gif.split(".")[0]
    imgEl.src = `./gifs/${id}.gif`
    newDiv.appendChild(imgEl)
    imgEl.addEventListener("click", handleCardClick);
    imgEl.classList.add("hide")
    imgEl.classList.remove("show")

    // if (newWidth < 768 && (count % 4 === 0 && count !== 0)) {
    //   temp = firstColor
    //   firstColor = secondColor
    //   secondColor = temp
    //   temp = ""
    // }

    if (count % 4 === 0 && count !== 0) {
      temp = firstColor
      firstColor = secondColor
      secondColor = temp
      temp = ""
    }

    if (count % 2 === 0) {
      newDiv.style.backgroundColor = firstColor
    }
    else {
      newDiv.style.backgroundColor = secondColor

    }
    gameContainer.append(newDiv);
    count += 1

    // if (newWidth > 768 && (count === 23)) {
    //   const newDiv = document.createElement("div");
    //   let imgEl = document.createElement("img")
    //   newDiv.style.backgroundColor = firstColor
    //   gameContainer.append(newDiv);

    // }





  }

  gamePageEl.appendChild(gameContainer)
}





// TODO: Implement this function!

let clickCounter = 0

function handleCardClick(event) {
  clickCounter += 1
  if (clickCounter === 1) {
    firstImage = event.target
    firstImage.removeEventListener("click", handleCardClick)
    firstImage.classList.add("show")
    firstImage.classList.remove("hide")
  }
  else if (clickCounter === 2) {
    secondImage = event.target
    secondImage.classList.add("show")
    secondImage.classList.remove("hide")
    gameContainer.disable = true
    secondImage.removeEventListener("click", handleCardClick)
    if (firstImage.src === secondImage.src) {
      clickCounter = 0
      score += 1
      console.log(firstImage.src)
      addLocalStorage(score, highScore)
      scoreSpanEl.textContent = `${score}`
    }
    else {
      setTimeout(() => {
        clickCounter = 0
        firstImage.addEventListener("click", handleCardClick)
        secondImage.addEventListener("click", handleCardClick)
        firstImage.classList.remove("show")
        secondImage.classList.remove("show")
        firstImage.classList.add("hide")
        secondImage.classList.add("hide")
        gameContainer.disabled = false
      }, 500)
    }
  }
}




let addLocalStorage = (score, highScore) => {
  if (highScore <= score && score !== 0) {
    highScore = score
    console.log("im inside local");
    higscoreSpanEl.textContent = `${highScore}`
    newHighScoreText.textContent = "You have beaten your own record !!!"
    localStorage.setItem("highScore", score)
  }
}

let successPageFunction = (score, timeTaken) => {

  if (timeTaken <= 60 && score === 12) {
    modalImg.src = "./images/shaun.jpeg"
    successEl.textContent = "You have photo graphic memory like Dr.Shaun Murphy !!!!!"
  }
  else if (score === 12) {
    modalImg.src = "./images/super_brain.jpg"
    successEl.textContent = "Like a light from blackhole nothing can escape  your memory !!!!!"
  }
  else if (score > 8) {
    modalImg.src = "./images/Brain.png"
    successEl.textContent = " Your nearly made it !!!"
  }
  else if (score > 4) {
    modalImg.src = "./images/Brain.png"
    successEl.textContent = "Good attempt !!! you can do better !!!!"
  }
  else {
    modalImg.src = "./images/confused_brain.jpg"
    successEl.textContent = "Time to do some memory excercises !!! We know you are better than this!!"
  }

}

function counter() {

  let timer = setInterval(() => {
    timerCount -= 1
    counterEl.innerHTML = `Timer: ${timerCount} Sec`
    let timeTaken = gameTimer - timerCount;
    if (timerCount <= 0 || score === 12) {
      clearInterval(timer)
      addLocalStorage(score, highScore)
      successPageFunction(score, timeTaken)
      finalScoreEl.textContent = `Your Score : ${score}`
      timeTakenEl.textContent = `Time Taken : ${gameTimer - timerCount} Secs`
      modalContainerEl.classList.remove("hide-container")
    }
  }, 1000)


}




// function createDivsForGifs2(gifArray) {
//   for (let gif of gifArray) {
//     const flipDivEl = document.createElement("div");
//     flipDivEl.classList.add("flip-box")
//     const newDiv = document.createElement("div");
//     newDiv.classList.add("flip-box-inner")
//     const frontImageContainerEl = document.createElement("div")
//     const backImageContainerEl = document.createElement("div")
//     let frontImgEl = document.createElement("img")
//     frontImgEl.src = "./images/thinking_emoji.png"
//     frontImageContainerEl.append(frontImgEl)
//     frontImageContainerEl.classList.add("flip-box-front")



//     let backImgEl = document.createElement("img")
//     let id = gif.split(".")[0]
//     backImgEl.src = `./gifs/${id}.gif`
//     backImageContainerEl.appendChild(backImgEl)
//     backImageContainerEl.classList.add("flip-box-back")

//     newDiv.append(frontImageContainerEl, backImageContainerEl)

//     flipDivEl.append(newDiv)

//     // console.log(flipDivEl)




//     flipDivEl.addEventListener("click", handleCardClick2);

//     if (count === 12) {
//       gameContainer.append(counterEl)
//     }
//     gameContainer.append(flipDivEl);
//     count += 1
//   }
// }




// clickCounter = 0

// function handleCardClick2(event) {
//   // console.log(event);
//   // console.log(event.target);
//   clickCounter += 1
//   let flipEventEl = event.target.firstChild;
//   console.log(flipEventEl);
//   console.log("click triggered")
//   flipEventEl.classList.toggle("rotate");

//   if (clickCounter === 1) {
//     let firstParentEl = event.target
//     // console.log(firstParentEl)
//     firstImageSrc = firstParentEl.firstChild.lastChild.firstChild.src
//     console.log(firstImageSrc)
//     firstParentEl.removeEventListener("click", handleCardClick)
//     firstParentEl.disabled = true

//   }
//   else if (clickCounter === 2) {

//     let secondParentEl = event.target
//     secondImageSrc = secondParentEl.firstChild.lastChild.firstChild.src
//     console.log(secondImageSrc);
//     gameContainer.disable = true
//     flipEventEl.removeEventListener("click", handleCardClick)
//     if (firstImageSrc === secondImageSrc) {
//       console.log("i am match")
//       clickCounter = 0
//       score += 1
//       addLocalStorage(score, highScore)
//       scoreSpanEl.textContent = `${score}`
//     }
//     else {
//       setTimeout(() => {
//         console.log("i am not a match")
//         flipEventEl.classList.toggle("rotate");
//         clickCounter = 0
//         firstParentEl.addEventListener("click", handleCardClick)
//         secondParentEl.addEventListener("click", handleCardClick)
//         gameContainer.disabled = false
//       }, 300)
//     }
//   }
// }



